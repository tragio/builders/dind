FROM docker:stable-git

RUN set -eux \
    && apk add --no-cache \
        docker-compose \
        make
