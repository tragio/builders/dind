#!/usr/bin/sh

# fall through to CI variable for image name
if [ -z $CONTAINER_IMAGE_NAME ]; then
    CONTAINER_IMAGE_NAME=${CI_PROJECT_NAME:-"dind-builder"}
fi
# fall through to CI variable for image namespace
if [ -z $CONTAINER_IMAGE_NAMESPACE ]; then
    CONTAINER_IMAGE_NAMESPACE=$CI_PROJECT_NAMESPACE
fi
# if a namespace, join
if [ $CONTAINER_IMAGE_NAMESPACE ]; then
    CONTAINER_IMAGE_NAME=$CONTAINER_IMAGE_NAMESPACE/$CONTAINER_IMAGE_NAME
fi
# fall through to CI variable for image registry
if [ -z $CONTAINER_IMAGE_REGISTRY ]; then
    CONTAINER_IMAGE_REGISTRY=$CI_REGISTRY
fi
# if still no registry, do not join
if [ $CONTAINER_IMAGE_REGISTRY ]; then
    CONTAINER_IMAGE_NAME=$CONTAINER_IMAGE_REGISTRY/$CONTAINER_IMAGE_NAME
fi

export CONTAINER_IMAGE_NAME=$CONTAINER_IMAGE_NAME
export COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)
export COMMIT_TAG=$(git tag --points-at HEAD)
